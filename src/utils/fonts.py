import customtkinter as ctk

DEFAULT_FONT_FAMILY = "Verdana"
TITLE_FONT = ctk.CTkFont(family=DEFAULT_FONT_FAMILY, size=40, weight="bold")
DESC_FONT = ctk.CTkFont(family=DEFAULT_FONT_FAMILY, size=20, weight="bold")
SMALL_DESC_FONT = ctk.CTkFont(family=DEFAULT_FONT_FAMILY, size=14)
BUTTON_FONT = ctk.CTkFont(family=DEFAULT_FONT_FAMILY, size=16)
