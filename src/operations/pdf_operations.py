import os
import logging

import PyPDF2


def merge_pdf(input_files, output_file_path):
    pdf_merger = PyPDF2.PdfMerger()

    for file_path in input_files:
        with open(file_path, 'rb') as file:
            logging.debug(f'Adding file {file_path} to the PDF merger')
            pdf_merger.append(file)

    with open(output_file_path, 'wb') as merged_pdf:
        logging.debug(f'Creating the merged file named: {output_file_path} right now')
        pdf_merger.write(merged_pdf)


def split_pdf(input_file_path, output_directory, page_number, split_page_number):
    input_file_name = os.path.basename(input_file_path)
    split_file_name_1 = f'{input_file_name.replace(".pdf", "")}_split_part1.pdf'
    split_file_name_2 = f'{input_file_name.replace(".pdf", "")}_split_part2.pdf'
    with open(input_file_path, 'rb') as file:
        pdf_reader = PyPDF2.PdfReader(file)
        logging.debug(f'Reading file {input_file_path}')
        pdf_writer = PyPDF2.PdfWriter()

        for page_num in range(0, split_page_number):
            pdf_writer.add_page(pdf_reader.pages[page_num])

        with open(os.path.join(output_directory, split_file_name_1), 'wb') as output_file_1:
            logging.debug(
                f'Creating the split file named: {os.path.join(output_directory, split_file_name_1)} right now')
            pdf_writer.write(output_file_1)

        pdf_writer = PyPDF2.PdfWriter()

        for page_num in range(split_page_number, page_number):
            pdf_writer.add_page(pdf_reader.pages[page_num])

        with open(os.path.join(output_directory, split_file_name_2), 'wb') as output_file_2:
            logging.debug(
                f'Creating the split file named: {os.path.join(output_directory, split_file_name_2)} right now')
            pdf_writer.write(output_file_2)


def extract_single_page_from_pdf(input_file_path, output_directory, page_number_to_extract):
    _extract_page_from_pdf(input_file_name=os.path.basename(input_file_path),
                           input_file_path=input_file_path,
                           output_directory=output_directory,
                           page_number_to_extract=page_number_to_extract)


def extract_multiple_pages_from_pdf(input_file_path, output_directory, page_numbers_to_extract):
    input_file_name = os.path.basename(input_file_path)
    for page_number_to_extract in page_numbers_to_extract:
        _extract_page_from_pdf(input_file_name, input_file_path, output_directory, page_number_to_extract)


def extract_range_of_pages_from_pdf(input_file_path, output_directory, range_start_page_number, range_end_page_number):
    input_file_name = os.path.basename(input_file_path)
    extracted_file_name = f'{input_file_name.replace(".pdf", "")}_from_page_{range_start_page_number}_to_{range_end_page_number}.pdf'
    with open(input_file_path, 'rb') as file:
        pdf_reader = PyPDF2.PdfReader(file)
        logging.debug(f'Reading file {input_file_path}')
        pdf_writer = PyPDF2.PdfWriter()
        for page_num in range(range_start_page_number - 1, range_end_page_number):
            pdf_writer.add_page(pdf_reader.pages[page_num])
        with open(os.path.join(output_directory, extracted_file_name), 'wb') as output_file:
            logging.debug(
                f'Creating the extracted file named: {os.path.join(output_directory, extracted_file_name)} right now')
            pdf_writer.write(output_file)


def count_pages(file_path):
    with open(file_path, 'rb') as file:
        logging.debug(f'Reading file {file_path} and counting pages')
        return len(PyPDF2.PdfReader(file).pages)


def _extract_page_from_pdf(input_file_name, input_file_path, output_directory, page_number_to_extract):
    extracted_file_name = f'{input_file_name.replace(".pdf", "")}_page_{page_number_to_extract}.pdf'
    with open(input_file_path, 'rb') as file:
        pdf_reader = PyPDF2.PdfReader(file)
        logging.debug(f'Reading file {input_file_path}')
        pdf_writer = PyPDF2.PdfWriter()
        pdf_writer.add_page(pdf_reader.pages[page_number_to_extract - 1])
        with open(os.path.join(output_directory, extracted_file_name), 'wb') as output_file:
            logging.debug(
                f'Creating the extracted file named: {os.path.join(output_directory, extracted_file_name)} right now')
            pdf_writer.write(output_file)
