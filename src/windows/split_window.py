import tkinter as tk
from tkinter import filedialog
import customtkinter as ctk
from CTkMessagebox import CTkMessagebox
import logging

from ..widgets.int_spinbox import IntSpinbox
from ..operations.pdf_operations import count_pages, split_pdf


class SplitWindow(ctk.CTkToplevel):
    def __init__(self, parent):
        ctk.CTkToplevel.__init__(self, parent)
        from ..utils import center_and_size_window, DESC_FONT, SMALL_DESC_FONT, BUTTON_FONT, WIDTH, HEIGHT
        self.parent = parent
        self.title("Split PDFs")
        center_and_size_window(self)  # Set the window size and position

        # Create a scrollable frame
        self.frame = ctk.CTkScrollableFrame(self, orientation="vertical", width=WIDTH, height=HEIGHT, fg_color="gray14")
        self.frame.pack()

        # Common variables required to store necessary information
        self.file_to_split = ""
        self.page_number = None
        large_text_wrap_length = WIDTH - 50

        # Window UI
        ## Input file selection components
        self.file_label = ctk.CTkLabel(self.frame, text="Please select the PDF file to split:", font=DESC_FONT,
                                       wraplength=large_text_wrap_length)
        self.file_entry = ctk.CTkEntry(self.frame)
        self.select_file_button = ctk.CTkButton(self.frame, text="Select File", command=self.choose_file,
                                                font=BUTTON_FONT, width=150)

        ## Page to split on selection components
        self.file_page_size_label = ctk.CTkLabel(self.frame, text="", font=SMALL_DESC_FONT,
                                                 wraplength=large_text_wrap_length)
        self.file_page_size_split_selection_label = ctk.CTkLabel(self.frame, text="", font=SMALL_DESC_FONT,
                                                                 wraplength=large_text_wrap_length)
        self.split_page_spinbox = IntSpinbox(self.frame, min_v=1, max_v=1, step_size=1,
                                             command=self.validate_split_button)
        self.file_page_size_split_note_label = ctk.CTkLabel(self.frame,
                                                            text="Please note that pages between 1 and the number "
                                                                 "chosen (inclusive) will make up the first document, "
                                                                 "and the remaining ones the second one",
                                                            font=SMALL_DESC_FONT, wraplength=large_text_wrap_length)

        ## Output directory selection components
        self.output_label = ctk.CTkLabel(self.frame, text="Please select where to save the split files:",
                                         font=DESC_FONT, wraplength=large_text_wrap_length)
        self.output_entry = ctk.CTkEntry(self.frame)
        self.select_output_button = ctk.CTkButton(self.frame, text="Select Output", command=self.save_file,
                                                  font=BUTTON_FONT, width=150)

        ## Decision buttons
        self.cancel_button = ctk.CTkButton(self.frame, text="Cancel", command=self.cancel, font=BUTTON_FONT, width=150)
        self.split_button = ctk.CTkButton(self.frame, text="Split", command=self.split_file, state=ctk.DISABLED,
                                          font=BUTTON_FONT, width=150)

        ## Building the UI
        ### Input file selection components
        self.file_label.pack(pady=10)
        self.file_entry.pack(fill=ctk.X, padx=25)
        self.select_file_button.pack(pady=7.5, ipady=7.5)
        ### Page to split on selection components starts hidden
        ### Output directory selection components
        self.output_label.pack(pady=10)
        self.output_entry.pack(fill=ctk.X, padx=25)
        self.select_output_button.pack(pady=7.5, ipady=7.5)
        ### Decision buttons
        self.cancel_button.pack(side=ctk.LEFT, pady=(15, 7.5), ipady=7.5)
        self.split_button.pack(side=ctk.RIGHT, pady=(15, 7.5), ipady=7.5)

        self.protocol("WM_DELETE_WINDOW", self.on_close)

        logging.info("Opened the split window")

    def choose_file(self):
        file = filedialog.askopenfilename(
            title="Please select the pdf file to split",
            filetypes=[('PDF Files', '*.pdf')],
            initialdir="/")

        if file:
            self.file_to_split = file
            self.file_entry.delete(0, ctk.END)
            self.file_entry.insert(0, file)

            self.page_number = count_pages(file)  # count how many pages the document has
            logging.info("User selected the following file to split: %s (%d page(s))", self.file_to_split, self.page_number)
            self.file_page_size_label.configure(text=f"The file has {self.page_number} pages")
            self.file_page_size_label.pack(after=self.select_file_button, pady=(0, 10))
            self.file_page_size_split_selection_label.configure(text=f"Choose a page number between 1 and "
                                                                     f"{self.page_number - 1} to split the document on")
            self.file_page_size_split_selection_label.pack(after=self.file_page_size_label)
            self.split_page_spinbox.configure(min_v=1, max_v=self.page_number - 1)
            self.split_page_spinbox.pack(after=self.file_page_size_split_selection_label)
            self.file_page_size_split_note_label.pack(after=self.split_page_spinbox)

            self.validate_split_button()

    def save_file(self):
        file = filedialog.askdirectory(mustexist=True, initialdir="/")

        if file:
            self.output_entry.delete(0, ctk.END)
            self.output_entry.insert(0, file)
            logging.info("User selected the following path to save the split files: %s", self.output_entry.get())

            self.validate_split_button()

    def validate_split_button(self, *args):
        if self.split_page_spinbox.get() and self.output_entry.get() and self.file_to_split and self.page_number:
            self.split_button.configure(state=ctk.NORMAL)
        else:
            self.split_button.configure(state=ctk.DISABLED)

    def split_file(self):
        logging.info("Splitting file...")
        split_pdf(self.file_to_split, self.output_entry.get(), self.page_number, int(self.split_page_spinbox.get()))
        logging.info("PDF file split successfully!")
        _ = CTkMessagebox(title="Success", message="PDF file split successfully!", icon="check", option_1="OK")
        _.wait_window(_)
        logging.info("Closing the split window after a successful split")
        self.parent.deiconify()  # show the main window
        self.parent.focus_set()  # set focus back to the main window
        self.destroy()  # close the split window

    def cancel(self):
        logging.info("Closing the split window after canceling the split operation")
        self.parent.deiconify()  # show the main window
        self.parent.focus_set()  # set focus back to the main window
        self.destroy()  # close the split window

    def on_close(self):
        logging.info("Closing the split window")
        self.parent.deiconify()  # show the main window
        self.parent.focus_set()  # set focus back to the main window
        self.destroy()  # close the split window
