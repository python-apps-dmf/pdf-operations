import customtkinter as ctk
import logging

from .merge_window import MergeWindow
from .split_window import SplitWindow
from .extract_window import ExtractWindow


class MainWindow(ctk.CTk):
    def __init__(self, *args, **kwargs):
        ctk.CTk.__init__(self, *args, **kwargs)
        from ..utils import TITLE_FONT, DESC_FONT, BUTTON_FONT, center_and_size_window
        self.title("PDF Operations")
        center_and_size_window(self)  # Set the window size and position

        # Window UI
        ## Opening message
        welcome_label = ctk.CTkLabel(self, text="Welcome", font=TITLE_FONT)
        instruction_label = ctk.CTkLabel(self, text="Please select which PDF operation you'd like to perform",
                                         font=DESC_FONT)

        ## Different operations
        merge_button = ctk.CTkButton(self, text="Merge", command=self.open_merge_window, font=BUTTON_FONT, width=150)
        split_button = ctk.CTkButton(self, text="Split", command=self.open_split_window, font=BUTTON_FONT, width=150)
        extract_button = ctk.CTkButton(self, text="Extract", command=self.open_extract_window, font=BUTTON_FONT,
                                       width=150)

        ## Building the UI
        welcome_label.pack(pady=20)
        instruction_label.pack(pady=10)
        merge_button.pack(pady=15, ipady=7.5)
        split_button.pack(pady=15, ipady=7.5)
        extract_button.pack(pady=15, ipady=7.5)

        self.protocol("WM_DELETE_WINDOW", self.on_close)

        logging.info("Started the PDF Operations app")

    def open_merge_window(self):
        self.withdraw()
        logging.info("Choosing the merge operation")
        MergeWindow(self)

    def open_split_window(self):
        self.withdraw()
        logging.info("Choosing the split operation")
        SplitWindow(self)

    def open_extract_window(self):
        self.withdraw()
        logging.info("Choosing the extract operation")
        ExtractWindow(self)

    def on_close(self):
        logging.info("Shutting down the app")
        self.destroy()
