from tkinter import filedialog, StringVar
import customtkinter as ctk
from CTkMessagebox import CTkMessagebox
import logging

from src.operations.pdf_operations import count_pages, extract_single_page_from_pdf, extract_multiple_pages_from_pdf, \
    extract_range_of_pages_from_pdf
from src.widgets import IntSpinbox
from src.windows.utils import are_all_string_var_number_elements_unique

SINGLE_PAGE_EXTRACTION = 'single'
MULTIPLE_PAGES_EXTRACTION = 'multiple'
RANGE_EXTRACTION = 'range'


class ExtractWindow(ctk.CTkToplevel):
    def __init__(self, parent):
        ctk.CTkToplevel.__init__(self, parent)
        from ..utils import center_and_size_window, DESC_FONT, BUTTON_FONT, SMALL_DESC_FONT, WIDTH, HEIGHT
        self.parent = parent
        self.title("Extract PDFs")
        center_and_size_window(self)  # Set the window size and position

        # Create a scrollable frame
        self.frame = ctk.CTkScrollableFrame(self, orientation="vertical", width=WIDTH, height=HEIGHT, fg_color="gray14")
        self.frame.pack()

        # Common variables required to store necessary information
        self.file_to_extract = ""
        self.page_number = None
        large_text_wrap_length = WIDTH - 50

        # Window UI
        ## Input file selection components
        self.file_label = ctk.CTkLabel(self.frame, text="Please select the PDF file to extract from:", font=DESC_FONT,
                                       wraplength=large_text_wrap_length)
        self.file_entry = ctk.CTkEntry(self.frame)
        self.select_file_button = ctk.CTkButton(self.frame, text="Select File", command=self.choose_file,
                                                font=BUTTON_FONT, width=150)
        self.file_page_size_label = ctk.CTkLabel(self.frame, text="", font=SMALL_DESC_FONT,
                                                 wraplength=large_text_wrap_length)

        ## Output directory selection components
        self.output_label = ctk.CTkLabel(self.frame, text="Please select where to save the extracted file(s):",
                                         font=DESC_FONT, wraplength=large_text_wrap_length)
        self.output_entry = ctk.CTkEntry(self.frame)
        self.select_output_button = ctk.CTkButton(self.frame, text="Select Output", command=self.save_file,
                                                  font=BUTTON_FONT, width=150)

        ## Type of extraction selection components
        self.extract_type_label = ctk.CTkLabel(self.frame, text="Select the type of extraction:", font=DESC_FONT,
                                               wraplength=large_text_wrap_length)
        self.extract_type_var = StringVar()
        self.single_page_radio = ctk.CTkRadioButton(self.frame, text="Single Page", variable=self.extract_type_var,
                                                    value=SINGLE_PAGE_EXTRACTION, command=self.update_extract_widgets,
                                                    state=ctk.DISABLED, font=BUTTON_FONT)
        self.multiple_pages_radio = ctk.CTkRadioButton(self.frame, text="Multiple Pages",
                                                       variable=self.extract_type_var,
                                                       value=MULTIPLE_PAGES_EXTRACTION,
                                                       command=self.update_extract_widgets, state=ctk.DISABLED,
                                                       font=BUTTON_FONT)
        self.range_pages_radio = ctk.CTkRadioButton(self.frame, text="Range of Pages", variable=self.extract_type_var,
                                                    value=RANGE_EXTRACTION, command=self.update_extract_widgets,
                                                    state=ctk.DISABLED, font=BUTTON_FONT)

        ## Extract single page components
        self.extract_single_page_label = ctk.CTkLabel(self.frame, text="", font=DESC_FONT,
                                                      wraplength=large_text_wrap_length)
        self.extract_single_page_spinbox = IntSpinbox(self.frame, min_v=1, max_v=1, step_size=1,
                                                      command=self.validate_extract_button)

        ## Extract multiple pages components
        self.choose_number_of_pages_to_extract_label = ctk.CTkLabel(self.frame,
                                                                    text="How many pages would you like to extract ("
                                                                         "max 10)", font=DESC_FONT,
                                                                    wraplength=large_text_wrap_length)
        self.choose_number_of_pages_to_extract_spinbox = (
            IntSpinbox(self.frame, min_v=1, max_v=10, step_size=1,
                       command=self.activate_multiple_pages_extraction_components_part2))
        self.extract_multiple_page_label = ctk.CTkLabel(self.frame, text="Select the individual pages to extract:",
                                                        font=DESC_FONT, wraplength=large_text_wrap_length)
        self.extract_pages_spinboxes = []

        ## Extract range of pages components
        self.range_start_page_label = ctk.CTkLabel(self.frame, text="", font=DESC_FONT,
                                                   wraplength=large_text_wrap_length)
        self.range_start_page_spinbox = IntSpinbox(self.frame, min_v=1, max_v=1, step_size=1,
                                                   command=self.validate_extract_button)
        self.range_end_page_label = ctk.CTkLabel(self.frame, text="", font=DESC_FONT, wraplength=large_text_wrap_length)
        self.range_end_page_spinbox = IntSpinbox(self, min_v=2, max_v=2, default_v=1, step_size=1,
                                                 command=self.validate_extract_button)

        ## Decision buttons
        self.cancel_button = ctk.CTkButton(self.frame, text="Cancel", command=self.cancel, font=BUTTON_FONT, width=150)
        self.extract_button = ctk.CTkButton(self.frame, text="Extract", command=self.extract_file, state=ctk.DISABLED,
                                            font=BUTTON_FONT, width=150)

        ## Building the UI
        ### Input file selection components
        self.file_label.pack(pady=10)
        self.file_entry.pack(fill=ctk.X, padx=25)
        self.select_file_button.pack(pady=7.5, ipady=7.5)
        ### Output directory selection components
        self.output_label.pack(pady=10)
        self.output_entry.pack(fill=ctk.X, padx=25)
        self.select_output_button.pack(pady=7.5, ipady=7.5)
        ### Type of extraction selection components
        self.extract_type_label.pack(pady=10)
        self.single_page_radio.pack(pady=(0, 2.5))
        self.multiple_pages_radio.pack(pady=2.5)
        self.range_pages_radio.pack(pady=(2.5, 0))
        ### Extract single page components starts hidden
        ### Extract multiple pages components starts hidden
        ### Extract range of pages components starts hidden
        ### Decision buttons
        self.cancel_button.pack(side=ctk.LEFT, pady=(15, 7.5), ipady=7.5)
        self.extract_button.pack(side=ctk.RIGHT, pady=(15, 7.5), ipady=7.5)

        self.protocol("WM_DELETE_WINDOW", self.on_close)

        logging.info("Opened the extract window")

    def choose_file(self):
        file = filedialog.askopenfilename(
            title="Please select the pdf file to extract from",
            filetypes=[('PDF Files', '*.pdf')],
            initialdir="/")

        if file:
            self.file_to_extract = file
            self.file_entry.delete(0, ctk.END)
            self.file_entry.insert(0, file)

            self.page_number = count_pages(file)  # count how many pages the document has
            logging.info("User selected the following file to extract from: %s (%d page(s))", self.file_to_extract, self.page_number)
            self.file_page_size_label.configure(text=f"The file has {self.page_number} pages")
            self.file_page_size_label.pack(after=self.select_file_button)

            self.single_page_radio.configure(state=ctk.NORMAL)
            self.multiple_pages_radio.configure(state=ctk.NORMAL)
            self.range_pages_radio.configure(state=ctk.NORMAL)

            self.extract_single_page_spinbox.configure(min_v=1, max_v=self.page_number)
            self.range_start_page_spinbox.configure(min_v=1, max_v=self.page_number - 1)
            self.range_end_page_spinbox.configure(min_v=2, max_v=self.page_number)

            self.update_extract_widgets()

    def update_extract_widgets(self):
        if self.extract_type_var.get():
            logging.info("User selected the %s operation", self.extract_type_var.get())
        if self.extract_type_var.get() == SINGLE_PAGE_EXTRACTION:  # single page extraction
            self.deactivate_multiple_pages_extraction_components()
            self.deactivate_range_of_pages_extraction_components()
            self.activate_single_page_extraction_components()
        elif self.extract_type_var.get() == MULTIPLE_PAGES_EXTRACTION:  # multiple pages extraction
            self.deactivate_single_page_extraction_components()
            self.deactivate_range_of_pages_extraction_components()
            self.activate_multiple_pages_extraction_components_part1()
        elif self.extract_type_var.get() == RANGE_EXTRACTION:  # range of pages extraction
            self.deactivate_single_page_extraction_components()
            self.deactivate_multiple_pages_extraction_components()
            self.activate_range_of_pages_extraction_components()

        self.validate_extract_button()

    def activate_single_page_extraction_components(self):
        self.extract_single_page_label.configure(text=f"Select the page to extract (min: 1, max: {self.page_number}):")
        self.extract_single_page_label.pack(after=self.range_pages_radio, pady=10)
        self.extract_single_page_spinbox.pack(after=self.extract_single_page_label)

    def deactivate_single_page_extraction_components(self):
        self.extract_single_page_label.pack_forget()
        self.extract_single_page_spinbox.set(value=0)
        self.extract_single_page_spinbox.pack_forget()

    def activate_multiple_pages_extraction_components_part1(self):
        self.choose_number_of_pages_to_extract_label.pack(after=self.range_pages_radio, pady=10)
        self.choose_number_of_pages_to_extract_spinbox.pack(after=self.choose_number_of_pages_to_extract_label)

    def activate_multiple_pages_extraction_components_part2(self, *args):
        if int(self.choose_number_of_pages_to_extract_spinbox.get()) != 0:  # the
            # deactivate_multiple_pages_extraction_components method indirectly sets this value to 0 so this is
            # protection
            self.extract_multiple_page_label.pack(after=self.choose_number_of_pages_to_extract_spinbox, pady=10)
            if len(self.extract_pages_spinboxes) > 0:
                for spinbox in self.extract_pages_spinboxes:
                    spinbox.pack_forget()
                self.extract_pages_spinboxes = []
            previous_spinbox = None
            for i in range(int(self.choose_number_of_pages_to_extract_spinbox.get())):
                spinbox = IntSpinbox(self.frame, min_v=1, max_v=self.page_number, step_size=1,
                                     command=self.validate_extract_button)
                self.extract_pages_spinboxes.append(spinbox)
                if i == 0:
                    spinbox.pack(after=self.extract_multiple_page_label)
                else:
                    spinbox.pack(after=previous_spinbox, pady=(5, 0))
                previous_spinbox = spinbox

    def deactivate_multiple_pages_extraction_components(self):
        self.choose_number_of_pages_to_extract_label.pack_forget()
        self.choose_number_of_pages_to_extract_spinbox.set(value=0)
        self.choose_number_of_pages_to_extract_spinbox.pack_forget()
        self.extract_multiple_page_label.pack_forget()
        if len(self.extract_pages_spinboxes) > 0:
            for spinbox in self.extract_pages_spinboxes:
                spinbox.pack_forget()
            self.extract_pages_spinboxes = []

    def activate_range_of_pages_extraction_components(self):
        self.range_start_page_label.configure(
            text=f"Select the page to start the extraction at (min: 1, max: {self.page_number - 1}):")
        self.range_start_page_label.pack(after=self.range_pages_radio, pady=10)
        self.range_start_page_spinbox.pack(after=self.range_start_page_label)
        self.range_end_page_label.configure(
            text=f"Select the page to end the extraction at (min: 2, max: {self.page_number}):")
        self.range_end_page_label.pack(after=self.range_start_page_spinbox, pady=5)
        self.range_end_page_spinbox.pack(after=self.range_end_page_label)

    def deactivate_range_of_pages_extraction_components(self):
        self.range_start_page_label.pack_forget()
        self.range_start_page_spinbox.set(value=0)
        self.range_start_page_spinbox.pack_forget()
        self.range_end_page_label.pack_forget()
        self.range_start_page_spinbox.set(value=1)
        self.range_end_page_spinbox.pack_forget()

    def validate_extract_button(self, *args):
        if (self.file_entry.get() and self.output_entry.get() and
                # single page extraction
                (self.extract_type_var.get() == SINGLE_PAGE_EXTRACTION and self.extract_single_page_spinbox.get())
                # multiple pages extraction
                or (self.extract_type_var.get() == MULTIPLE_PAGES_EXTRACTION and
                    len(self.extract_pages_spinboxes) > 0 and
                    all(spinbox_var.get() for spinbox_var in self.extract_pages_spinboxes) and
                    are_all_string_var_number_elements_unique(self.extract_pages_spinboxes))
                # range of pages extraction
                or (self.extract_type_var.get() == RANGE_EXTRACTION and
                    self.range_start_page_spinbox.get() and
                    self.range_end_page_spinbox.get() and
                    int(self.range_start_page_spinbox.get()) < int(self.range_end_page_spinbox.get()))):
            self.extract_button.configure(state=ctk.NORMAL)
        else:
            self.extract_button.configure(state=ctk.DISABLED)

    def save_file(self):
        file = filedialog.askdirectory(mustexist=True, initialdir="/")

        if file:
            self.output_entry.delete(0, ctk.END)
            self.output_entry.insert(0, file)
            logging.info("User selected the following path to save the extracted files: %s", self.output_entry.get())

            self.validate_extract_button()

    def extract_file(self):
        logging.info("Extracting from file (%s operation)...", self.extract_type_var.get())
        if self.extract_type_var.get() == SINGLE_PAGE_EXTRACTION:  # single page extraction
            extract_single_page_from_pdf(self.file_to_extract, self.output_entry.get(),
                                         int(self.extract_single_page_spinbox.get()))
        elif self.extract_type_var.get() == MULTIPLE_PAGES_EXTRACTION:  # multiple pages extraction
            extract_multiple_pages_from_pdf(self.file_to_extract, self.output_entry.get(),
                                            [int(var.get()) for var in self.extract_pages_spinboxes])
        elif self.extract_type_var.get() == RANGE_EXTRACTION:  # range of pages extraction
            extract_range_of_pages_from_pdf(self.file_to_extract, self.output_entry.get(),
                                            int(self.range_start_page_spinbox.get()),
                                            int(self.range_end_page_spinbox.get()))
        _ = CTkMessagebox(title="Success", message="Pages were successfully extracted from the PDF file!", icon="check",
                          option_1="OK")
        logging.info("Pages were successfully extracted from the PDF file!")
        _.wait_window(_)
        logging.info("Closing the extract window after a successful extraction")
        self.parent.deiconify()  # show the main window
        self.parent.focus_set()  # set focus back to the main window
        self.destroy()  # close the extract window

    def cancel(self):
        logging.info("Closing the extract window after cancelling the extraction operation")
        self.parent.deiconify()  # show the main window
        self.parent.focus_set()  # set focus back to the main window
        self.destroy()  # close the extract window

    def on_close(self):
        logging.info("Closing the extract window")
        self.parent.deiconify()  # show the main window
        self.parent.focus_set()  # set focus back to the main window
        self.destroy()  # close the extract window
