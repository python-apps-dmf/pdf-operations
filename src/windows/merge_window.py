from tkinter import filedialog
import customtkinter as ctk
from CTkMessagebox import CTkMessagebox
from CTkListbox import CTkListbox
import logging

from ..operations.pdf_operations import merge_pdf


class MergeWindow(ctk.CTkToplevel):
    def __init__(self, parent):
        ctk.CTkToplevel.__init__(self, parent)
        from ..utils import center_and_size_window, DESC_FONT, BUTTON_FONT, WIDTH, HEIGHT
        self.parent = parent
        self.title("Merge PDFs")
        center_and_size_window(self)  # Set the window size and position

        # Create a scrollable frame
        self.frame = ctk.CTkScrollableFrame(self, orientation="vertical", width=WIDTH, height=HEIGHT, fg_color="gray14")
        self.frame.pack()

        # Common variables required to store necessary information
        self.files_to_merge = []
        large_text_wrap_length = WIDTH - 50

        # Window UI
        ## Input files selection components
        self.file_label = ctk.CTkLabel(self.frame, text="Please select the PDF files to merge:", font=DESC_FONT,
                                       wraplength=large_text_wrap_length)
        self.file_listbox = CTkListbox(self.frame)
        self.select_files_button = ctk.CTkButton(self.frame, text="Select Files", command=self.choose_files,
                                                 font=BUTTON_FONT, width=150)

        ## Output file name and directory selection components
        self.output_label = ctk.CTkLabel(self.frame, text="Please select the name and where to save the merged file:",
                                         font=DESC_FONT, wraplength=large_text_wrap_length)
        self.output_entry = ctk.CTkEntry(self.frame)
        self.select_output_button = ctk.CTkButton(self.frame, text="Select Output", command=self.save_file,
                                                  font=BUTTON_FONT, width=150)

        ## Decision buttons
        self.cancel_button = ctk.CTkButton(self.frame, text="Cancel", command=self.cancel)
        self.merge_button = ctk.CTkButton(self.frame, text="Merge", command=self.merge_files, state=ctk.DISABLED)

        ## Building the UI
        ### Input files selection components
        self.file_label.pack(pady=10)
        self.file_listbox.pack(fill=ctk.X, padx=25)
        self.select_files_button.pack(pady=7.5, ipady=7.5)
        ### Output file name and directory selection components
        self.output_label.pack(pady=10)
        self.output_entry.pack(fill=ctk.X, padx=25)
        self.select_output_button.pack(pady=7.5, ipady=7.5)
        ### Decision buttons
        self.cancel_button.pack(side=ctk.LEFT, pady=(15, 7.5), ipady=7.5)
        self.merge_button.pack(side=ctk.RIGHT, pady=(15, 7.5), ipady=7.5)

        self.protocol("WM_DELETE_WINDOW", self.on_close)

        logging.info("Opened the merge window")

    def choose_files(self):
        files = filedialog.askopenfilenames(title="Please select the pdf files to merge",
                                            filetypes=[('PDF Files', '*.pdf')],
                                            initialdir="/")

        if files:
            self.files_to_merge.extend(files)  # extend the existing list with new files
            if self.file_listbox.size() > 0:
                self.file_listbox.delete(0, ctk.END)

            for file in self.files_to_merge:
                self.file_listbox.insert(ctk.END, file)

            logging.info("User selected the following files to merge: %s", self.files_to_merge)
            if len(self.files_to_merge) > 1 and self.output_entry.get():
                self.merge_button.configure(state=ctk.NORMAL)

    def save_file(self):
        file = filedialog.asksaveasfilename(defaultextension=".pdf", filetypes=[("PDF files", "*.pdf")])

        if file:
            self.output_entry.delete(0, ctk.END)
            self.output_entry.insert(0, file)

            logging.info("User selected the following path to save the merged file: %s", self.output_entry.get())
            if len(self.files_to_merge) > 1:
                self.merge_button.configure(state=ctk.NORMAL)

    def merge_files(self):
        logging.info("Merging files...")
        merge_pdf(self.files_to_merge, self.output_entry.get())
        logging.info("PDF files merged successfully!")
        _ = CTkMessagebox(title="Success", message="PDF files merged successfully!", icon="check", option_1="OK")
        _.wait_window(_)
        logging.info("Closing the merge window after a successful merge")
        self.parent.deiconify()  # show the main window
        self.parent.focus_set()  # set focus back to the main window
        self.destroy()  # close the merge window

    def cancel(self):
        logging.info("Closing the merge window after canceling the merge operation")
        self.parent.deiconify()  # show the main window
        self.parent.focus_set()  # set focus back to the main window
        self.destroy()  # close the merge window

    def on_close(self):
        logging.info("Closing the merge window")
        self.parent.deiconify()  # show the main window
        self.parent.focus_set()  # set focus back to the main window
        self.destroy()  # close the merge window
