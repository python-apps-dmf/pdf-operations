def are_all_string_var_number_elements_unique(string_vars):
    values = [int(var.get()) for var in string_vars]
    return len(values) == len(set(values))
