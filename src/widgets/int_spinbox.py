from typing import Union, Callable

import customtkinter


class IntSpinbox(customtkinter.CTkFrame):
    def __init__(self, *args,
                 width: int = 100,
                 height: int = 32,
                 step_size: Union[int, int] = 1,
                 min_v: int = 0,
                 max_v: int = 50000,
                 default_v: int = 0,
                 command: Callable = None,
                 **kwargs):
        if max_v < min_v:
            raise ValueError(f"max_v: {max_v} must be larger than min_v: {min_v}")

        super().__init__(*args, width=width, height=height, **kwargs)

        self.step_size = step_size
        self.command = command
        self.min_v = min_v
        self.max_v = max_v

        self.configure(fg_color=("gray78", "gray28"))  # set frame color

        self.grid_columnconfigure((0, 2), weight=0)  # buttons don't expand
        self.grid_columnconfigure(1, weight=1)  # entry expands

        self.subtract_button = customtkinter.CTkButton(self, text="-", width=height - 6, height=height - 6,
                                                       command=self.subtract_button_callback)
        self.subtract_button.grid(row=0, column=0, padx=(3, 0), pady=3)

        self.entry = customtkinter.CTkEntry(self, width=width - (2 * height), height=height - 6, border_width=0)
        self.entry.grid(row=0, column=1, columnspan=1, padx=3, pady=3, sticky="ew")

        self.add_button = customtkinter.CTkButton(self, text="+", width=height - 6, height=height - 6,
                                                  command=self.add_button_callback)
        self.add_button.grid(row=0, column=2, padx=(0, 3), pady=3)

        # default value
        self.entry.insert(0, str(default_v))
        # self.entry.bind(sequence='<KeyRelease>', command=self.set)

    def add_button_callback(self):
        if self.check_limits(int(self.entry.get()) + self.step_size):
            try:
                value = int(self.entry.get()) + self.step_size
                self.entry.delete(0, "end")
                self.entry.insert(0, value)
                if self.command is not None:
                    self.command()
            except ValueError:
                return

    def subtract_button_callback(self):
        if self.check_limits(int(self.entry.get()) - self.step_size):
            try:
                value = int(self.entry.get()) - self.step_size
                self.entry.delete(0, "end")
                self.entry.insert(0, value)
                if self.command is not None:
                    self.command()
            except ValueError:
                return

    def get(self) -> Union[int, None]:
        try:
            return int(self.entry.get())
        except ValueError:
            return None

    def set(self, value: int):
        if self.check_limits(value):
            self.entry.delete(0, "end")
            self.entry.insert(0, str(int(value)))

    def check_limits(self, value):
        return self.min_v <= value <= self.max_v

    def configure(self, require_redraw=False, **kwargs):
        if "min_v" in kwargs:
            self.min_v = int(kwargs.pop("min_v"))

        if "max_v" in kwargs:
            self.max_v = int(kwargs.pop("max_v"))

        super().configure(require_redraw=require_redraw, **kwargs)
