# PDF Operations

This python project is meant to store code that can create an executable desktop application with a GUI to execute multiple pdf operations. The user should be able to select both the source and the output PDF file(s).

## Operations supported/to support

Supported:

- Merge
- Split
- Extract single page
- Extract multiple pages separately
- Extract a range of pages

To Support:


- Extract text content (transcript)
- Generate audio file
- Summarize file contents (maybe also in audio format)

## Packages in use

- PyPDF2
- customtkinter
- CTkMessagebox
- CTkListbox

## Structure

- `/pdf-operations` - root directory 
  - `/src` - contains the application source code
  - `/notebooks` - contains the Jupyter notebooks used to create the application
  - `/data` - contains the notebook experiment data

## How to use

1. First clone the project
2. Create a python virtual environment in the root folder `python -m venv .venv`
3. Turn the virtual environment on with `.\.venv\Scripts\activate`
4. Install the project dependencies with `pip install -r .\requirements.txt`
   1. If prompted, you can upgrade pip with `python -m pip install --upgrade pip`
5. Run the command `python -m src.main`, this should start the application
   1. Note: you may have to change the alias used to run python depending on your OS (like 'python3')
